﻿using LankaBookin.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace LankaBookin.DAL
{
    public class LankaBookinEntities : DbContext
    {
        public LankaBookinEntities()
            : base("LankaBookinEntities")
        {

        }

        public DbSet<AccomadationType> AccomadationType { get; set; }
        public DbSet<Apartment> Apartment { get; set; }
        //public DbSet<ApartmentAccomadation> ApartmentAccomadation { get; set; }
        //public DbSet<ApartmentFacility> ApartmentFacility { get; set; }
        public DbSet<ApartmentImage> ApartmentImage { get; set; }
        public DbSet<BookingRequest> BookingRequest { get; set; }
        public DbSet<Category> Category { get; set; }
        public DbSet<CategoryTour> CategoryTour { get; set; }
        public DbSet<City> City { get; set; }
        public DbSet<District> District { get; set; }
        public DbSet<Facility> Facility { get; set; }
        public DbSet<Landmark> Landmark { get; set; }
        public DbSet<Rating> Rating { get; set; }
        public DbSet<RoomsBook> RoomsBook { get; set; }
        public DbSet<Tour> Tour { get; set; }
        public DbSet<TourDestination> TourDestination { get; set; }
        public DbSet<TourDestinationRequest> TourDestinationRequest { get; set; }
        public DbSet<TourRequest> TourRequest { get; set; }
        public DbSet<UserWishList> UserWishList { get; set; }
        public DbSet<User> User { get; set; }
        public DbSet<Role> Roles { get; set; }
        public DbSet<UsersInRole> UsersInRoles { get; set; }
    }
}