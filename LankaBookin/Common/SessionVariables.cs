﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LankaBookin.Common
{
    public enum SessionVariables
    {
        UserId,

        Search,

        Checkin,

        Checkout,

        Duration,

        ApartmentId,

        TourId,

        CategoryId,

        TourMode,

        TourRequest,

        TourDestinationRequests,

        TourRequestId
    }

    public enum TourMode
    {
        On = 1,

        Off = 0
    }

    public enum UserRoles
    {
        SuperAdmin,

        Admin,

        Public,

        Internal
    }
}