﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using LankaBookin.Models;
using LankaBookin.DAL;

namespace LankaBookin.Controllers
{
    [Authorize(Roles = "Admin,SuperAdmin")]
    public class DistrictController : Controller
    {
        private LankaBookinEntities db = new LankaBookinEntities();

        //
        // GET: /District/

        public ActionResult Index()
        {
            return View(db.District.Where(d=>d.Deleted!=true).ToList());
        }

        //
        // GET: /District/Details/5

        public ActionResult Details(int id = 0)
        {
            District lb_district = db.District.Find(id);
            if (lb_district == null)
            {
                return HttpNotFound();
            }
            ViewBag.City = db.City.Where(c => c.District.Id == id).ToList();
            return View(lb_district);
        }

        //
        // GET: /District/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /District/Create

        [HttpPost]
        public ActionResult Create(District lb_district)
        {
            if (ModelState.IsValid)
            {
                lb_district.Deleted = false;
                int userId = Convert.ToInt32(Session[Common.SessionVariables.UserId.ToString()]);
                lb_district.CreatedBy = userId;
                lb_district.CreatedDate = DateTime.Now;
                db.District.Add(lb_district);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(lb_district);
        }

        //
        // GET: /District/Edit/5

        public ActionResult Edit(int id = 0)
        {
            District lb_district = db.District.Find(id);
            if (lb_district == null)
            {
                return HttpNotFound();
            }
            return View(lb_district);
        }

        //
        // POST: /District/Edit/5

        [HttpPost]
        public ActionResult Edit(District lb_district)
        {
            if (ModelState.IsValid)
            {
                int userId = Convert.ToInt32(Session[Common.SessionVariables.UserId.ToString()]);
                lb_district.ModifiedBy = userId;
                lb_district.ModifiedDate = DateTime.Now;
                db.Entry(lb_district).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(lb_district);
        }

        //
        // GET: /District/Delete/5
        [Authorize(Roles = "SuperAdmin")]
        public ActionResult Delete(int id = 0)
        {
            District lb_district = db.District.Find(id);
            if (lb_district == null)
            {
                return HttpNotFound();
            }
            return View(lb_district);
        }

        //
        // POST: /District/Delete/5

        [HttpPost, ActionName("Delete")]
        [Authorize(Roles = "SuperAdmin")]
        public ActionResult DeleteConfirmed(int id)
        {
            District lb_district = db.District.Find(id);
            lb_district.Deleted = true;
            db.Entry(lb_district).State = EntityState.Modified;
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}