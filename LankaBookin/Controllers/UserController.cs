﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using LankaBookin.Models;
using System.Web.Security;
using WebMatrix.WebData;
using LankaBookin.DAL;

namespace LankaBookin.Controllers
{
    [Authorize(Roles = "Admin,SuperAdmin")]
    public class UserController : Controller
    {
        private LankaBookinEntities db = new LankaBookinEntities();

        //
        // GET: /User/

        public ActionResult Index()
        {
            return View(db.User.Where(u=>u.Id!=1).ToList());
        }

        //
        // GET: /User/Details/5

        public ActionResult Details(int id = 0)
        {
            User lb_user = db.User.Find(id);
            Role userRole = (from ur in db.UsersInRoles
                             join role in db.Roles on ur.RoleId equals role.RoleId
                             where ur.UserId == id
                             select role).SingleOrDefault();

            ViewBag.UserRole = userRole;

            if (lb_user == null)
            {
                return HttpNotFound();
            }
            return View(lb_user);
        }

        //
        // GET: /User/Create

        public ActionResult Create()
        {
            List<Role> userRoles = db.Roles.Where(r=>r.RoleId!=1).ToList();
            ViewBag.UserRoles = userRoles;
            return View();
        }

        //
        // POST: /User/Create

        [HttpPost]
        public ActionResult Create(User lb_user)
        {
            if (ModelState.IsValid)
            {
                WebSecurity.CreateUserAndAccount(lb_user.UserName, "123456", propertyValues: new
                {
                    Name = lb_user.Name,
                    Address = lb_user.Address,
                    Phone = lb_user.Phone,
                    Email = lb_user.Email,
                    Deleted=false,
                });

                List<Role> userRoles = db.Roles.ToList();
                string roleName = string.Empty;
                foreach (Role role in userRoles)
                {
                    var result = Request[Convert.ToString(role.RoleId)];
                    if (result != null)
                    {
                        if (result.ToString()=="on")
                        {
                            roleName = role.RoleName;
                        }
                    }

                }
                if (!string.IsNullOrEmpty(roleName))
                {
                    if (!Roles.RoleExists(roleName))
                        Roles.CreateRole(roleName);
                    Roles.AddUserToRole(lb_user.UserName, roleName);
                }

                return RedirectToAction("Index");
            }

            return View(lb_user);
        }

        //
        // GET: /User/Edit/5
        [Authorize(Roles="SuperAdmin")]
        public ActionResult Edit(int id = 0)
        {
            User lb_user = db.User.Find(id);
            if (lb_user == null)
            {
                return HttpNotFound();
            }
            return View(lb_user);
        }

        //
        // POST: /User/Edit/5

        [HttpPost]
        [Authorize(Roles = "SuperAdmin")]
        public ActionResult Edit(User lb_user)
        {
            if (ModelState.IsValid)
            {
                db.Entry(lb_user).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(lb_user);
        }

        //
        // GET: /User/Delete/5
        [Authorize(Roles = "SuperAdmin")]
        public ActionResult Delete(int id = 0)
        {
            User lb_user = db.User.Find(id);
            if (lb_user == null)
            {
                return HttpNotFound();
            }
            return View(lb_user);
        }

        //
        // POST: /User/Delete/5
        [Authorize(Roles = "SuperAdmin")]
        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            User lb_user = db.User.Find(id);
            db.User.Remove(lb_user);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}