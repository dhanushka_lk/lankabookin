﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using LankaBookin.Models;
using LankaBookin.DAL;

namespace LankaBookin.Controllers
{
    [Authorize(Roles = "Admin,SuperAdmin")]
    public class TourController : Controller
    {
        private LankaBookinEntities db = new LankaBookinEntities();

        //
        // GET: /Tour/
        [AllowAnonymous]
        public ActionResult PublicTourList(int categoryId=-1)
        {
            if (categoryId != -1)
            {
                var result = (from cat_tour in db.CategoryTour
                              join tour in db.Tour on cat_tour.TourId equals tour.Id
                              join cat in db.Category on cat_tour.CategoryId equals cat.Id
                              where cat_tour.CategoryId == categoryId && tour.Deleted!=true
                              select tour).ToList();

                Session[Common.SessionVariables.CategoryId.ToString()] = categoryId;
                return View(result);
            }

            return View(db.Tour.Where(t=>t.Deleted!=true).ToList());
        }

        //
        // GET: /Tour/
        public ActionResult Index()
        {
            return View(db.Tour.Where(t=>t.Deleted!=true).ToList());
        }


        //
        // GET: /Tour/Details/5

        public ActionResult Details(int id = 0)
        {
            Tour lb_tour = db.Tour.Find(id);
            if (lb_tour == null)
            {
                return HttpNotFound();
            }

            ViewBag.Destinations = (from tour_des in db.TourDestination
                          join tour in db.Tour on tour_des.TourId equals tour.Id
                          join city in db.City on tour_des.CityId equals city.Id
                          where city.Deleted != true && tour.Id == id
                          select city).ToList();

            return View(lb_tour);
        }

        //
        // GET: /Tour/PublicDetails/5
        [AllowAnonymous]
        public ActionResult PublicDetails(int id = 0)
        {
            Tour lb_tour = db.Tour.Find(id);
            if (lb_tour == null)
            {
                return HttpNotFound();
            }

            ViewBag.Destinations = (from tour_des in db.TourDestination
                                    join tour in db.Tour on tour_des.TourId equals tour.Id
                                    join city in db.City on tour_des.CityId equals city.Id
                                    where city.Deleted != true && tour.Id == id
                                    select city).ToList();

            return View(lb_tour);
        }

        //
        // GET: /Tour/Create

        public ActionResult Create(int step=0)
        {
            ViewBag.CategoryTypes = new SelectList(db.Category.Where(C => C.Deleted != true), "Id", "CategoryName");
            ViewBag.Cities = new SelectList(db.City.Where(C => C.Deleted != true), "Id", "CityName");
            return View();
        }

        //
        // POST: /Tour/Create

        [HttpPost]
        public ActionResult Create(Tour lb_tour)
        {
            if (ModelState.IsValid)
            {
                int userId = Convert.ToInt32(Session[Common.SessionVariables.UserId.ToString()]);
                lb_tour.Deleted = false;
                lb_tour.CreatedDate = DateTime.Now;
                lb_tour.CreatedBy = userId;
                db.Tour.Add(lb_tour);
                db.SaveChanges();

                var result = Request["CategoryTypes"];
                CategoryTour category_tour = new CategoryTour
                {
                    CategoryId = Convert.ToInt32(result),
                    TourId = lb_tour.Id
                };
                db.CategoryTour.Add(category_tour);
                db.SaveChanges();

                var districts = Request["Cities"];

                int counter = 1;
                if (districts != null)
                {
                    foreach (var d in districts.ToList())
                    {
                        int districtId = (int)Char.GetNumericValue(d);
                        if (districtId == -1)
                            continue;
                        TourDestination tour_des = new TourDestination
                        {
                            TourId=lb_tour.Id,
                            StepId=counter,
                            CityId = districtId,
                            IsDesCity=true,
                            Deleted=false,
                            CreatedBy = userId,
                            CreatedDate=DateTime.Now
                        };
                        db.TourDestination.Add(tour_des);
                        db.SaveChanges();
                        counter++;
                    }
                }

                return RedirectToAction("Index");
            }

            return View(lb_tour);
        }

        //
        // GET: /Tour/Edit/5

        public ActionResult Edit(int id = 0)
        {
            Tour lb_tour = db.Tour.Find(id);
            if (lb_tour == null)
            {
                return HttpNotFound();
            }
            return View(lb_tour);
        }

        //
        // POST: /Tour/Edit/5

        [HttpPost]
        public ActionResult Edit(Tour lb_tour)
        {
            if (ModelState.IsValid)
            {
                int userId = Convert.ToInt32(Session[Common.SessionVariables.UserId.ToString()]);
                lb_tour.ModifiedBy = userId;
                lb_tour.ModifiedDate = DateTime.Now;
                db.Entry(lb_tour).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(lb_tour);
        }

        //
        // GET: /Tour/Delete/5

        [Authorize(Roles = "SuperAdmin")]
        public ActionResult Delete(int id = 0)
        {
            Tour lb_tour = db.Tour.Find(id);
            if (lb_tour == null)
            {
                return HttpNotFound();
            }
            return View(lb_tour);
        }

        //
        // POST: /Tour/Delete/5

        [Authorize(Roles = "SuperAdmin")]
        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            Tour lb_tour = db.Tour.Find(id);
            lb_tour.Deleted = true;
            db.Entry(lb_tour).State = EntityState.Modified;
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}