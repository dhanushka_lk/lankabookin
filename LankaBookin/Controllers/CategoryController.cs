﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using LankaBookin.Models;
using LankaBookin.DAL;

namespace LankaBookin.Controllers
{
    [Authorize(Roles = "Admin,SuperAdmin")]
    public class CategoryController : Controller
    {
        private LankaBookinEntities db = new LankaBookinEntities();

        //
        // GET: /Category/

        public ActionResult Index()
        {
            return View(db.Category.Where(c=>c.Deleted!=true).ToList());
        }

        //
        // GET: /Category/Details/5

        public ActionResult Details(int id = 0)
        {
            Category lb_category = db.Category.Find(id);
            if (lb_category == null)
            {
                return HttpNotFound();
            }

            var tours = (from tour in db.Tour
                         join cat_tour in db.CategoryTour on tour.Id equals cat_tour.TourId
                         where cat_tour.CategoryId == id
                         select tour).ToList();

            ViewBag.Tours = tours;

            return View(lb_category);
        }

        //
        // GET: /Category/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /Category/Create

        [HttpPost]
        public ActionResult Create(Category lb_category)
        {
            if (ModelState.IsValid)
            {
                int userId = Convert.ToInt32(Session[Common.SessionVariables.UserId.ToString()]);
                lb_category.CreatedBy = userId;
                lb_category.CreatedDate = DateTime.Now;
                lb_category.Deleted = false;
                db.Category.Add(lb_category);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(lb_category);
        }

        //
        // GET: /Category/Edit/5

        public ActionResult Edit(int id = 0)
        {
            Category lb_category = db.Category.Find(id);
            if (lb_category == null)
            {
                return HttpNotFound();
            }
            return View(lb_category);
        }

        //
        // POST: /Category/Edit/5

        [HttpPost]
        public ActionResult Edit(Category lb_category)
        {
            if (ModelState.IsValid)
            {
                int userId = Convert.ToInt32(Session[Common.SessionVariables.UserId.ToString()]);
                lb_category.ModifiedBy = userId;
                lb_category.ModifiedDate = DateTime.Now;
                db.Entry(lb_category).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(lb_category);
        }

        //
        // GET: /Category/Delete/5
        [Authorize(Roles = "SuperAdmin")]
        public ActionResult Delete(int id = 0)
        {
            Category lb_category = db.Category.Find(id);
            if (lb_category == null)
            {
                return HttpNotFound();
            }
            return View(lb_category);
        }

        //
        // POST: /Category/Delete/5
        [Authorize(Roles = "SuperAdmin")]
        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            Category lb_category = db.Category.Find(id);
            lb_category.Deleted = true;
            db.Entry(lb_category).State = EntityState.Modified;
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}