﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using LankaBookin.Models;
using LankaBookin.DAL;

namespace LankaBookin.Controllers
{
    [Authorize]
    public class UserWishListController : Controller
    {
        private LankaBookinEntities db = new LankaBookinEntities();

        //
        // GET: /UserWishList/

        public ActionResult Index()
        {
            int userId = Convert.ToInt32(Session[Common.SessionVariables.UserId.ToString()]);
            if (userId == 0)
            {
                return RedirectToAction("Index","Home",null);
            }
            var lb_userwishlist = db.UserWishList.Where(l => l.UserId == userId);
            return View(lb_userwishlist.ToList());
        }

        //
        // GET: /UserWishList/Delete/5

        public ActionResult Delete(int id = 0)
        {
            UserWishList lb_userwishlist = db.UserWishList.Find(id);
            if (lb_userwishlist == null)
            {
                return HttpNotFound();
            }
            return View(lb_userwishlist);
        }

        //
        // POST: /UserWishList/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            UserWishList lb_userwishlist = db.UserWishList.Find(id);
            db.UserWishList.Remove(lb_userwishlist);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}