﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using LankaBookin.Models;
using LankaBookin.DAL;

namespace LankaBookin.Controllers
{
    [Authorize]
    public class TourRequestController : Controller
    {
        private LankaBookinEntities db = new LankaBookinEntities();

        //
        // GET: /TourRequest/

        public ActionResult Index()
        {
            return View(db.TourRequest.Where(t=>t.Deleted!=true).ToList());
        }

        //
        // GET: /TourRequest/Details/5

        public ActionResult Details(int id = 0)
        {
            TourRequest lb_tourrequest = db.TourRequest.Find(id);
            if (lb_tourrequest == null)
            {
                return HttpNotFound();
            }
            return View(lb_tourrequest);
        }

        //
        // GET: /TourRequest/Create
        [Authorize(Roles="Public")]
        public ActionResult Create(int tourId=0)
        {
            Tour lb_tour = db.Tour.Find(tourId);
            if (lb_tour == null)
            {
                return HttpNotFound();
            }

            ViewBag.Tour = lb_tour;
            ViewBag.CityList = new SelectList(db.City, "CityId", "City");

            return View();
        }

        //
        // POST: /TourRequest/Create

        [HttpPost]
        [Authorize(Roles = "Public")]
        public ActionResult Create(TourRequest lb_tourrequest)
        {
            if (ModelState.IsValid)
            {
                int steps = 0;
                var stepsResult = (from tour in db.Tour
                                   where tour.Id == lb_tourrequest.TourId
                                   select tour).SingleOrDefault().Steps;

                int userId = Convert.ToInt32(Session[Common.SessionVariables.UserId.ToString()]);

                TourRequest tourRequest = new TourRequest
                {
                    Deleted=false,
                    TourId=lb_tourrequest.TourId,
                    Duration=lb_tourrequest.Duration,
                    UserId = userId,
                    CreatedBy = userId,
                    CreatedDate=DateTime.Now
                };

                db.TourRequest.Add(tourRequest);
                db.SaveChanges();

                Tour lb_tour = db.Tour.Find(lb_tourrequest.TourId);

                steps = Convert.ToInt32(stepsResult);
                for (int x = 1; x < steps + 1; x++)
                {
                    DateTime checkin=DateTime.MinValue;
                    DateTime checkout = DateTime.MinValue;
                    int apartmentId=0;
                    decimal price=0;
                    int duration = 0;
                    DateTime orderDate = DateTime.Now;

                    try
                    {
                        checkin =Convert.ToDateTime(Request["Checkin" + x]);
                        checkout =Convert.ToDateTime(Request["Checkout" + x]);
                        apartmentId =Convert.ToInt32(Request["sa" + x]);
                        price =Convert.ToDecimal(Request["price" + x]);
                        duration = Convert.ToDateTime(checkout).Subtract(Convert.ToDateTime(checkout)).Days;
                    }
                    catch (Exception)
                    {

                    }

                    BookingRequest bookingRequest = new BookingRequest
                    {
                        ApartmentId=apartmentId,
                        Price=price,
                        UserId=userId,
                        CheckIn=checkin,
                        CheckOut=checkout,
                        OrderDate=orderDate,
                        RoomsBooked=0,
                        Deleted=false,
                        CreatedBy=userId,
                        CreatedDate=DateTime.Now
                    };

                    db.BookingRequest.Add(bookingRequest);
                    db.SaveChanges();

                    int cityId = lb_tour.TourDestination.ElementAt(x-1).CityId;

                    TourDestinationRequest tour_des_req = new TourDestinationRequest
                    {
                        StepId = x,
                        duration = Convert.ToInt32(duration),
                        BookingRequestId=bookingRequest.Id,
                        TourRequestId=tourRequest.Id,
                        CityId = cityId,
                        Deleted=false,
                        CreatedBy=userId,
                        CreatedDate=DateTime.Now
                    };

                    db.TourDestinationRequest.Add(tour_des_req);
                    db.SaveChanges();

                }

                return RedirectToAction("Index", "Home", null);
            }

            return View(lb_tourrequest);
        }

        //
        // GET: /TourRequest/Edit/5

        public ActionResult Edit(int id = 0)
        {
            TourRequest lb_tourrequest = db.TourRequest.Find(id);
            if (lb_tourrequest == null)
            {
                return HttpNotFound();
            }
            ViewBag.TourId = new SelectList(db.Tour, "TourId", "Tour", lb_tourrequest.TourId);
            return View(lb_tourrequest);
        }

        //
        // POST: /TourRequest/Edit/5

        [HttpPost]
        public ActionResult Edit(TourRequest lb_tourrequest)
        {
            if (ModelState.IsValid)
            {
                db.Entry(lb_tourrequest).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.TourId = new SelectList(db.Tour, "TourId", "Tour", lb_tourrequest.TourId);
            return View(lb_tourrequest);
        }

        //
        // GET: /TourRequest/Delete/5

        public ActionResult Delete(int id = 0)
        {
            TourRequest lb_tourrequest = db.TourRequest.Find(id);
            if (lb_tourrequest == null)
            {
                return HttpNotFound();
            }
            return View(lb_tourrequest);
        }

        //
        // POST: /TourRequest/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            TourRequest lb_tourrequest = db.TourRequest.Find(id);
            db.TourRequest.Remove(lb_tourrequest);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}