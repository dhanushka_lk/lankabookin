﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using LankaBookin.Models;
using LankaBookin.DAL;

namespace LankaBookin.Controllers
{
    [Authorize(Roles = "Admin,SuperAdmin")]
    public class AdminApartmentController : Controller
    {
        private LankaBookinEntities db = new LankaBookinEntities();

        //
        // GET: /AdminApartment/

        public ActionResult Index()
        {
            var apartment = db.Apartment.Where(a=>a.Deleted!=true);
            return View(apartment.ToList());
        }

        //
        // GET: /AdminApartment/Details/5

        public ActionResult Details(int id = 0)
        {
            Apartment apartment = db.Apartment.Find(id);
            if (apartment == null)
            {
                return HttpNotFound();
            }
            return View(apartment);
        }

        //
        // GET: /AdminApartment/Create

        public ActionResult Create()
        {
            var internalUsers = (from ur in db.UsersInRoles
                                 join role in db.Roles on ur.RoleId equals role.RoleId
                                 join user in db.User on ur.UserId equals user.Id
                                 where role.RoleId == 4
                                 select user).ToList();

            ViewBag.CityId = new SelectList(db.City, "Id", "CityName");
            ViewBag.UserId = new SelectList(internalUsers, "Id", "Name");
            return View();
        }

        //
        // POST: /AdminApartment/Create

        [HttpPost]
        public ActionResult Create(Apartment apartment)
        {
            if (ModelState.IsValid)
            {
                int userId = Convert.ToInt32(Session[Common.SessionVariables.UserId.ToString()]);
                apartment.Deleted = false;
                apartment.CreatedDate = DateTime.Now;
                apartment.CreatedBy = userId;
                db.Apartment.Add(apartment);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            var internalUsers = (from ur in db.UsersInRoles
                                 join role in db.Roles on ur.RoleId equals role.RoleId
                                 join user in db.User on ur.UserId equals user.Id
                                 where role.RoleId == 4
                                 select user).ToList();
            ViewBag.CityId = new SelectList(db.City, "Id", "CityName", apartment.CityId);
            ViewBag.UserId = new SelectList(internalUsers, "Id", "Name", apartment.UserId);
            return View(apartment);
        }

        //
        // GET: /AdminApartment/Edit/5

        public ActionResult Edit(int id = 0)
        {
            Apartment apartment = db.Apartment.Find(id);
            if (apartment == null)
            {
                return HttpNotFound();
            }
            ViewBag.CityId = new SelectList(db.City, "Id", "CityName", apartment.CityId);
            ViewBag.UserId = new SelectList(db.User, "Id", "Name", apartment.UserId);
            return View(apartment);
        }

        //
        // POST: /AdminApartment/Edit/5

        [HttpPost]
        public ActionResult Edit(Apartment apartment)
        {
            if (ModelState.IsValid)
            {
                int userId = Convert.ToInt32(Session[Common.SessionVariables.UserId.ToString()]);
                apartment.ModifiedBy = userId;
                apartment.ModifiedDate = DateTime.Now;
                db.Entry(apartment).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.CityId = new SelectList(db.City, "Id", "CityName", apartment.CityId);
            ViewBag.UserId = new SelectList(db.User, "Id", "Name", apartment.UserId);
            return View(apartment);
        }

        //
        // GET: /AdminApartment/Delete/5
        [Authorize(Roles = "SuperAdmin")]
        public ActionResult Delete(int id = 0)
        {
            Apartment apartment = db.Apartment.Find(id);
            if (apartment == null)
            {
                return HttpNotFound();
            }
            return View(apartment);
        }

        //
        // POST: /AdminApartment/Delete/5
        [Authorize(Roles = "SuperAdmin")]
        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            Apartment apartment = db.Apartment.Find(id);
            apartment.Deleted = true;
            db.Entry(apartment).State = EntityState.Modified;
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}