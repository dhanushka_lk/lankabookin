﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using LankaBookin.Models;
using LankaBookin.DAL;
using WebMatrix.WebData;

namespace LankaBookin.Controllers
{
    [Authorize(Roles = "Internal")]
    public class RoomsBookController : Controller
    {
        private LankaBookinEntities db = new LankaBookinEntities();

        //
        // GET: /RoomsBook/

        public ActionResult Index()
        {
            int userId = WebSecurity.GetUserId(User.Identity.Name);
            return View(db.RoomsBook.Where(r=>r.Apartment.UserId==userId).ToList());
        }

        //
        // GET: /RoomsBook/Create

        public ActionResult Create()
        {
            int userId = WebSecurity.GetUserId(User.Identity.Name);
            var apartments = db.Apartment.Where(a => a.UserId == userId && a.Deleted == false).ToList();
            ViewBag.ApartmentId = new SelectList(apartments, "Id", "Name");
            return View();
        }

        //
        // POST: /RoomsBook/Create

        [HttpPost]
        public ActionResult Create(RoomsBook roomsbook)
        {
            int userId = WebSecurity.GetUserId(User.Identity.Name);

            if (ModelState.IsValid)
            {
                roomsbook.CreatedBy = userId;
                roomsbook.CreatedDate = DateTime.Now;
                roomsbook.ModifiedBy = userId;
                roomsbook.ModifiedDate = DateTime.Now;

                db.RoomsBook.Add(roomsbook);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            var apartments = db.Apartment.Where(a => a.UserId == userId && a.Deleted == false).ToList();
            ViewBag.ApartmentId = new SelectList(apartments, "Id", "Name");
            return View(roomsbook);
        }

        //
        // GET: /RoomsBook/Edit/5

        public ActionResult Edit(int id = 0)
        {
            RoomsBook roomsbook = db.RoomsBook.Find(id);
            if (roomsbook == null)
            {
                return HttpNotFound();
            }

            ViewBag.ApartmentId = new SelectList(db.Apartment, "Id", "Name", roomsbook.ApartmentId);
            return View(roomsbook);
        }

        //
        // POST: /RoomsBook/Edit/5

        [HttpPost]
        public ActionResult Edit(RoomsBook roomsbook)
        {
            if (ModelState.IsValid)
            {
                int userId = WebSecurity.GetUserId(User.Identity.Name);
                roomsbook.ModifiedBy = userId;
                roomsbook.ModifiedDate = DateTime.Now;
                db.Entry(roomsbook).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.ApartmentId = new SelectList(db.Apartment, "Id", "Name", roomsbook.ApartmentId);
            return View(roomsbook);
        }

        //
        // GET: /RoomsBook/Delete/5

        public ActionResult Delete(int id = 0)
        {
            RoomsBook roomsbook = db.RoomsBook.Find(id);
            if (roomsbook == null)
            {
                return HttpNotFound();
            }
            return View(roomsbook);
        }

        //
        // POST: /RoomsBook/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            RoomsBook roomsbook = db.RoomsBook.Find(id);
            db.RoomsBook.Remove(roomsbook);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}