﻿using LankaBookin.DAL;
using LankaBookin.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace LankaBookin.Controllers
{
    public class HomeController : Controller
    {
        private LankaBookinEntities db = new LankaBookinEntities();

        public ActionResult Index()
        {
            if (User.IsInRole(Common.UserRoles.Admin.ToString()) || User.IsInRole(Common.UserRoles.SuperAdmin.ToString()))
            {
                return RedirectToAction("Index", "User");
            }
            else if (User.IsInRole(Common.UserRoles.Internal.ToString()))
            {
                return RedirectToAction("Index", "Apartment");
            }

            ViewBag.Category = db.Category.ToList();
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your app description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";
            return View();
        }
    }
}
