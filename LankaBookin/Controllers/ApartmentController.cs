﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using LankaBookin.Models;
using LankaBookin.DAL;
using WebMatrix.WebData;
using System.IO;

namespace LankaBookin.Controllers
{
    [Authorize]
    public class ApartmentController : Controller
    {
        private LankaBookinEntities db = new LankaBookinEntities();

        //
        // GET: /Apartment/
        [Authorize]
        [Authorize(Roles = "Internal")]
        public ActionResult Index()
        {
            int userId = WebSecurity.GetUserId(User.Identity.Name);
            return View(db.Apartment.Where(a => a.UserId == userId && a.Deleted != true).ToList());
        }

        [Authorize]
        public ActionResult ApartmentList(DateTime? checkin = null, DateTime? checkout = null,int desId=0)
        {
            ViewBag.DesId = desId;
            return PartialView(db.Apartment.Where(a => a.Deleted != true).ToList());
        }

        //
        // GET: /Apartment/
        [AllowAnonymous]
        public ActionResult Search(string search = "", DateTime? checkin = null, DateTime? checkout = null, int AccomadationTypes = 0)
        {
            List<Apartment> apartments;
            List<Apartment> apartmentList;

            if (!string.IsNullOrEmpty(search))
            {
                apartments = db.Apartment.Where(a => a.Deleted != true && a.Name.Contains(search)).ToList();
                apartmentList = db.Apartment.Where(a => a.Deleted != true && a.Name.Contains(search)).ToList();
            }
            else
            {
                apartments = db.Apartment.Where(a => a.Deleted != true).ToList();
                apartmentList = db.Apartment.Where(a => a.Deleted != true).ToList();
            }

            foreach (var apartment in apartments)
            {
                if (checkin != null && checkout != null)
                {
                    DateTime date=(DateTime)checkin;
                    while(date<=checkout)
                    {
                        var result = db.RoomsBook.Where(r => r.ApartmentId == apartment.Id && r.Date == date).Select(r=>r.RoomCount).Count();
                        if (result > 0)
                        {
                            if (!apartment.IsPerRoom)
                            {
                                apartmentList.Remove(apartment);
                            }
                            else
                            {
                                if (apartment.RoomCount <= result)
                                {
                                    apartmentList.Remove(apartment);
                                }
                            }
                        }

                        date = date.AddDays(1);
                    }

                }
            }

            ViewBag.AccomadationTypes = new SelectList(db.AccomadationType, "Id", "AccomadationName");
            ViewBag.CityId = new SelectList(db.City, "Id", "CityName");
            List<Facility> facilities = db.Facility.ToList();
            ViewBag.Facilities = facilities;

            return View(apartmentList);
        }

        [AllowAnonymous]
        public ActionResult DetailsPartial(int id = 0)
        {
            Apartment lb_apartment = db.Apartment.Find(id);
            if (lb_apartment == null)
            {
                return HttpNotFound();
            }

            return PartialView(lb_apartment); ;
        }

        //
        // GET: /Apartment/Details/5
        public ActionResult Details(int id = 0)
        {
            Apartment lb_apartment = db.Apartment.Find(id);
            if (lb_apartment == null)
            {
                return HttpNotFound();
            }
            return View(lb_apartment);
        }

        //
        // GET: /Apartment/Details/5
        [Authorize(Roles = "Internal")]
        public ActionResult InternalDetails(int id = 0)
        {
            Apartment lb_apartment = db.Apartment.Find(id);
            if (lb_apartment == null)
            {
                return HttpNotFound();
            }
            return View(lb_apartment);
        }

        //
        // GET: /Apartment/Create
        [Authorize(Roles = "Internal")]
        public ActionResult Create()
        {
            ViewBag.CityId = new SelectList(db.City, "Id", "CityName");
            List<AccomadationType> accomadationTypes = db.AccomadationType.ToList();
            ViewBag.AccomadationTypes = accomadationTypes;
            List<Facility> facilities = db.Facility.ToList();
            ViewBag.Facilities = facilities;
            return View();
        }

        //
        // POST: /Apartment/Create

        [HttpPost]
        [Authorize(Roles = "Internal")]
        public ActionResult Create(Apartment apartment, HttpPostedFileBase[] files)
        {
            if (files.Count() > 15)
            {
                ModelState.AddModelError("upload", "Maxmimum upload count is 15 images");
            }

            if (ModelState.IsValid)
            {
                int userId = WebSecurity.GetUserId(User.Identity.Name);

                if (apartment.UserId == 0)
                {
                    apartment.UserId = userId;
                }

                apartment.Deleted = false;
                apartment.CreatedDate = DateTime.Now;
                apartment.CreatedBy = userId;
                db.Apartment.Add(apartment);
                db.SaveChanges();

                List<AccomadationType> accomadationTypes = db.AccomadationType.ToList();
                foreach (AccomadationType type in accomadationTypes)
                {
                    var result = Request[Convert.ToString("Acc"+type.Id)];
                    if (result != null)
                    {
                        if (result.ToString() == "on")
                        {
                            apartment.Accomadations.Add(type);
                            db.SaveChanges();
                        }
                    }

                }

                List<Facility> facilities = db.Facility.ToList();
                foreach (Facility type in facilities)
                {
                    var result = Request[Convert.ToString("Fac" + type.Id)];
                    if (result != null)
                    {
                        if (result.ToString() == "on")
                        {
                            apartment.Facilities.Add(type);
                            db.SaveChanges();
                        }
                    }

                }

                foreach (HttpPostedFileBase file in files)
                {
                    if (file != null)
                    {
                        string pic = System.IO.Path.GetFileName(file.FileName);
                        string path = Server.MapPath("~/Images/Apartment/"+apartment.Name);
                        string fullPath = System.IO.Path.Combine(path, pic);

                        if (!Directory.Exists(path))
                        {
                            DirectoryInfo di = Directory.CreateDirectory(path);
                        }

                        // file is uploaded
                        file.SaveAs(fullPath);

                    }
                }

                return RedirectToAction("Index");
            }

            ViewBag.CityId = new SelectList(db.City, "Id", "CityName", apartment.CityId);
            List<AccomadationType> accomadationType = db.AccomadationType.ToList();
            ViewBag.AccomadationTypes = accomadationType; 
            List<Facility> facility = db.Facility.ToList();
            ViewBag.Facilities = facility;
            return View(apartment);
        }

        //
        // GET: /Apartment/Edit/5
        [Authorize(Roles = "Internal")]
        public ActionResult Edit(int id = 0)
        {
            int userId = WebSecurity.GetUserId(User.Identity.Name);
            Apartment apartment = db.Apartment.Where(a => a.Id == id && a.UserId == userId).SingleOrDefault();
            
            if (apartment == null)
            {
                return HttpNotFound();
            }

            ViewBag.CityId = new SelectList(db.City, "Id", "CityName", apartment.CityId);

            List<AccomadationType> accomadationType = db.AccomadationType.ToList();
            ViewBag.AccomadationTypes = accomadationType;

            List<Facility> facility = db.Facility.ToList();
            ViewBag.Facilities = facility;
            return View(apartment);
        }

        //
        // POST: /Apartment/Edit/5

        [HttpPost]
        [Authorize(Roles = "Internal")]
        public ActionResult Edit(Apartment apartment, HttpPostedFileBase[] files, string[] selectedAccomadationTypes, string[] selectedFacilities)
        {
            string fpath = Server.MapPath("~/Images/Apartment/" + apartment.Name);
            int fCount = Directory.GetFiles(fpath, "*", SearchOption.AllDirectories).Length;

            if (files.Count() + fCount > 15)
            {
                ModelState.AddModelError("upload", "Maxmimum upload count is 15 images");
            }

            if (ModelState.IsValid)
            {
                apartment.ModifiedDate = DateTime.Now;
                db.Entry(apartment).State = EntityState.Modified;
                db.SaveChanges();

                ICollection<AccomadationType> accomadationTypes = new HashSet<AccomadationType>();
                if (selectedAccomadationTypes != null)
                {
                    foreach (var accomadationType in db.AccomadationType.ToList())
                    {
                        if (selectedAccomadationTypes.ToList().Contains(accomadationType.Id.ToString()))
                        {
                            accomadationType.Apartments.Add(apartment);
                            accomadationTypes.Add(accomadationType);
                        }
                        else
                        {
                            accomadationType.Apartments.Remove(apartment);
                        }
                    }
                }
                else
                {
                    foreach (var accomadationType in db.AccomadationType.ToList())
                    {
                        accomadationType.Apartments.Remove(apartment);
                    }
                }

                apartment.Accomadations = accomadationTypes;
                db.SaveChanges();

                ICollection<Facility> facilities = new HashSet<Facility>();

                if (selectedFacilities != null)
                {
                    foreach (var facility in db.Facility.ToList())
                    {
                        if (selectedFacilities.ToList().Contains(facility.Id.ToString()))
                        {
                            facility.Apartments.Add(apartment);
                            facilities.Add(facility);
                        }
                        else
                        {
                            facility.Apartments.Remove(apartment);
                        }
                    }

                }
                else
                {
                    foreach (var facility in db.Facility.ToList())
                    {
                        facility.Apartments.Remove(apartment);
                    }
                }

                apartment.Facilities = facilities;
                db.SaveChanges();


                foreach (HttpPostedFileBase file in files)
                {
                    if (file != null)
                    {
                        string pic = System.IO.Path.GetFileName(file.FileName);
                        string path = Server.MapPath("~/Images/Apartment/" + apartment.Name);
                        string fullPath = System.IO.Path.Combine(path, pic);

                        if (!Directory.Exists(path))
                        {
                            DirectoryInfo di = Directory.CreateDirectory(path);
                        }

                        // file is uploaded
                        file.SaveAs(fullPath);

                    }
                }

                return RedirectToAction("Index");
            }
            ViewBag.CityId = new SelectList(db.City, "Id", "CityName", apartment.CityId);
            return View(apartment);
        }

        //
        // GET: /Apartment/Delete/5
        [Authorize(Roles = "Internal")]
        public ActionResult Delete(int id = 0)
        {
            Apartment apartment = db.Apartment.Find(id);
            if (apartment == null)
            {
                return HttpNotFound();
            }
            return View(apartment);
        }


        //
        // GET: /Apartment/WishList/5

        public ActionResult WishList(int id = 0)
        {
            if (id != 0)
            {
                if (User.IsInRole(Common.UserRoles.Public.ToString()))
                {
                    int userId = Convert.ToInt32(Session[Common.SessionVariables.UserId.ToString()]);
                    if (userId != -1)
                    {
                        UserWishList wishList = new UserWishList
                        {
                            ApartmentId = id,
                            UserId = userId
                        };
                        db.UserWishList.Add(wishList);
                        db.SaveChanges();
                    }
                }
            }

            return RedirectToAction("Search");
        }

        public ActionResult DeleteImage(string imagePath)
        {
            string path = Server.MapPath("~/" + imagePath);
            if (System.IO.File.Exists(path))
            {
                System.IO.File.Delete(path);
            }

            return Json(new { result = true }, JsonRequestBehavior.AllowGet);
        }

        //
        // POST: /Apartment/Delete/5

        [HttpPost, ActionName("Delete")]
        [Authorize(Roles = "Internal")]
        public ActionResult DeleteConfirmed(int id)
        {
            Apartment apartment = db.Apartment.Find(id);
            apartment.Deleted = true;
            db.Entry(apartment).State = EntityState.Modified;
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }

        public ActionResult GetApartmentName(int apartmentId)
        {
            Apartment apartment = db.Apartment.Find(apartmentId);
            return Json(new { name = apartment.Name,price=apartment.Price,isPerRoom=apartment.IsPerRoom }, JsonRequestBehavior.AllowGet);
        }
    }
}