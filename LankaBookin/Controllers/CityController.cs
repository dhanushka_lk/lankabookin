﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using LankaBookin.Models;
using LankaBookin.DAL;

namespace LankaBookin.Controllers
{
    [Authorize(Roles="Admin,SuperAdmin")]
    public class CityController : Controller
    {
        private LankaBookinEntities db = new LankaBookinEntities();

        //
        // GET: /City/

        public ActionResult Index()
        {
            return View(db.City.Where(c=>c.Deleted!=true).ToList());
        }

        //
        // GET: /City/Create

        public ActionResult Create()
        {
            ViewBag.DistrictId = new SelectList(db.District.Where(C => C.Deleted != true), "Id", "DistrictName");
            return View();
        }

        //
        // POST: /City/Create

        [HttpPost]
        public ActionResult Create(City lb_city)
        {
            if (ModelState.IsValid)
            {
                int userId = Convert.ToInt32(Session[Common.SessionVariables.UserId.ToString()]);
                lb_city.Deleted = false;
                lb_city.CreatedBy = userId;
                lb_city.CreatedDate = DateTime.Now;
                db.City.Add(lb_city);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(lb_city);
        }

        //
        // GET: /City/Edit/5

        public ActionResult Edit(int id = 0)
        {
            City lb_city = db.City.Find(id);
            if (lb_city == null)
            {
                return HttpNotFound();
            }
            ViewBag.Districts = new SelectList(db.District.Where(C => C.Deleted != true), "DistrictId", "District");
            return View(lb_city);
        }

        //
        // POST: /City/Edit/5

        [HttpPost]
        public ActionResult Edit(City lb_city)
        {
            if (ModelState.IsValid)
            {
                int userId = Convert.ToInt32(Session[Common.SessionVariables.UserId.ToString()]);
                lb_city.ModifiedBy = userId;
                lb_city.ModifiedDate = DateTime.Now;
                db.Entry(lb_city).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(lb_city);
        }

        //
        // GET: /City/Delete/5
        [Authorize(Roles="SuperAdmin")]
        public ActionResult Delete(int id = 0)
        {
            City lb_city = db.City.Find(id);
            if (lb_city == null)
            {
                return HttpNotFound();
            }
            return View(lb_city);
        }

        //
        // POST: /City/Delete/5

        [HttpPost, ActionName("Delete")]
        [Authorize(Roles = "SuperAdmin")]
        public ActionResult DeleteConfirmed(int id)
        {
            City lb_city = db.City.Find(id);
            lb_city.Deleted = true;
            db.Entry(lb_city).State = EntityState.Modified;
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}