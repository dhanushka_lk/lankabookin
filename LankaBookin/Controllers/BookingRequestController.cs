﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using LankaBookin.Models;
using LankaBookin.DAL;

namespace LankaBookin.Controllers
{
    [Authorize]
    public class BookingRequestController : Controller
    {
        private LankaBookinEntities db = new LankaBookinEntities();

        //
        // GET: /BookingRequest/

        public ActionResult Index()
        {
                int userId = Convert.ToInt32(Session[Common.SessionVariables.UserId.ToString()]);
                var lb_bookingrequest = db.BookingRequest.Where(b=>b.UserId==userId).ToList();
                return View(lb_bookingrequest.ToList());
        }

        //
        // GET: /BookingRequest/Create
        [Authorize(Roles = "Public")]
        public ActionResult Create(int id = 0)
        {
            Apartment lb_apartment = db.Apartment.Find(id);

            BookingRequest bookinRequest = new BookingRequest
            {
                OrderDate=DateTime.Now
            };

            var checkin = Session[Common.SessionVariables.Checkin.ToString()];
            if (checkin != null)
            {
                bookinRequest.CheckIn = (DateTime)checkin;
            }

            var checkout = Session[Common.SessionVariables.Checkout.ToString()];
            if (checkout != null)
            {
                bookinRequest.CheckOut = (DateTime)checkout;
            }

            bookinRequest.ApartmentId = lb_apartment.Id;
            bookinRequest.Apartment = lb_apartment;

            if (!lb_apartment.IsPerRoom)
            {
                bookinRequest.Price = lb_apartment.Price;
            }

            return View(bookinRequest);
        }

        //
        // POST: /BookingRequest/Create

        [HttpPost]
        [Authorize(Roles = "Public")]
        public ActionResult Create(BookingRequest lb_bookingrequest, int id)
        {
            if (ModelState.IsValid)
            {
                int userId = Convert.ToInt32(Session[Common.SessionVariables.UserId.ToString()]);
                lb_bookingrequest.UserId = userId;

                lb_bookingrequest.OrderDate = DateTime.Now;
                lb_bookingrequest.CreatedBy = userId;
                lb_bookingrequest.CreatedDate = DateTime.Now;
                lb_bookingrequest.Deleted = false;
                lb_bookingrequest.ApartmentId = id;
                db.BookingRequest.Add(lb_bookingrequest);
                db.SaveChanges();
                return RedirectToAction("Index","Home",null);
            }

            return View(lb_bookingrequest);
        }

        ////
        //// GET: /BookingRequest/Delete/5
        //[Authorize(Roles = "SuperAdmin")]
        //public ActionResult Delete(int id = 0)
        //{
        //    BookingRequest lb_bookingrequest = db.BookingRequest.Find(id);
        //    if (lb_bookingrequest == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(lb_bookingrequest);
        //}

        ////
        //// POST: /BookingRequest/Delete/5

        //[HttpPost, ActionName("Delete")]
        //[Authorize(Roles = "SuperAdmin")]
        //public ActionResult DeleteConfirmed(int id)
        //{
        //    BookingRequest lb_bookingrequest = db.BookingRequest.Find(id);
        //    db.BookingRequest.Remove(lb_bookingrequest);
        //    db.SaveChanges();
        //    return RedirectToAction("Index");
        //}

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }

        public ActionResult CalculatePrice(int apartmentId, int roomCount)
        {
            Apartment apartment = db.Apartment.Find(apartmentId);
            var price = apartment.Price * roomCount;
            return Json(new { price = price }, JsonRequestBehavior.AllowGet);
        }
    }
}