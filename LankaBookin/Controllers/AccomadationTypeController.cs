﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using LankaBookin.Models;
using LankaBookin.DAL;

namespace LankaBookin.Controllers
{
    [Authorize(Roles = "Admin,SuperAdmin")]
    public class AccomadationTypeController : Controller
    {
        private LankaBookinEntities db = new LankaBookinEntities();

        //
        // GET: /AccomadationType/

        public ActionResult Index()
        {
            return View(db.AccomadationType.Where(a=>a.Deleted!=true).ToList());
        }

        //
        // GET: /AccomadationType/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /AccomadationType/Create

        [HttpPost]
        public ActionResult Create(AccomadationType accomadationtype)
        {
            if (ModelState.IsValid)
            {
                int userId = Convert.ToInt32(Session[Common.SessionVariables.UserId.ToString()]);
                accomadationtype.CreatedBy = userId;
                accomadationtype.CreatedDate = DateTime.Now;
                accomadationtype.Deleted = false;
                db.AccomadationType.Add(accomadationtype);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(accomadationtype);
        }

        //
        // GET: /AccomadationType/Edit/5

        public ActionResult Edit(int id = 0)
        {
            AccomadationType lb_accomadationtype = db.AccomadationType.Find(id);
            if (lb_accomadationtype == null)
            {
                return HttpNotFound();
            }
            return View(lb_accomadationtype);
        }

        //
        // POST: /AccomadationType/Edit/5

        [HttpPost]
        public ActionResult Edit(AccomadationType accomadationtype)
        {
            if (ModelState.IsValid)
            {
                int userId = Convert.ToInt32(Session[Common.SessionVariables.UserId.ToString()]);
                accomadationtype.ModifiedDate = DateTime.Now;
                accomadationtype.ModifiedBy = userId;
                db.Entry(accomadationtype).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(accomadationtype);
        }

        //
        // GET: /AccomadationType/Delete/5
        [Authorize(Roles = "SuperAdmin")]
        public ActionResult Delete(int id = 0)
        {
            AccomadationType lb_accomadationtype = db.AccomadationType.Find(id);
            if (lb_accomadationtype == null)
            {
                return HttpNotFound();
            }
            return View(lb_accomadationtype);
        }

        //
        // POST: /AccomadationType/Delete/5

        [HttpPost, ActionName("Delete")]
        [Authorize(Roles = "SuperAdmin")]
        public ActionResult DeleteConfirmed(int id)
        {
            AccomadationType accomadationtype = db.AccomadationType.Find(id);
            accomadationtype.Deleted = true;
            db.Entry(accomadationtype).State = EntityState.Modified;
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}