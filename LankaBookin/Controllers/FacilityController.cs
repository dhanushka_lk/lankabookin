﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using LankaBookin.Models;
using LankaBookin.DAL;

namespace LankaBookin.Controllers
{
    [Authorize(Roles = "Admin,SuperAdmin")]
    public class FacilityController : Controller
    {
        private LankaBookinEntities db = new LankaBookinEntities();

        //
        // GET: /Facility/

        public ActionResult Index()
        {
            return View(db.Facility.Where(f=>f.Deleted!=true).ToList());
        }

        //
        // GET: /Facility/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /Facility/Create

        [HttpPost]
        public ActionResult Create(Facility lb_facility)
        {
            if (ModelState.IsValid)
            {
                lb_facility.Deleted = false;
                db.Facility.Add(lb_facility);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(lb_facility);
        }

        //
        // GET: /Facility/Edit/5

        public ActionResult Edit(int id = 0)
        {
            Facility lb_facility = db.Facility.Find(id);
            if (lb_facility == null)
            {
                return HttpNotFound();
            }
            return View(lb_facility);
        }

        //
        // POST: /Facility/Edit/5

        [HttpPost]
        public ActionResult Edit(Facility lb_facility)
        {
            if (ModelState.IsValid)
            {
                db.Entry(lb_facility).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(lb_facility);
        }

        //
        // GET: /Facility/Delete/5
        [Authorize(Roles = "SuperAdmin")]
        public ActionResult Delete(int id = 0)
        {
            Facility lb_facility = db.Facility.Find(id);
            if (lb_facility == null)
            {
                return HttpNotFound();
            }
            return View(lb_facility);
        }

        //
        // POST: /Facility/Delete/5

        [HttpPost, ActionName("Delete")]
        [Authorize(Roles = "SuperAdmin")]
        public ActionResult DeleteConfirmed(int id)
        {
            Facility lb_facility = db.Facility.Find(id);
            lb_facility.Deleted = true;
            db.Entry(lb_facility).State = EntityState.Modified;
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}