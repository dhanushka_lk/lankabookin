﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace LankaBookin.Models
{
    [Table("Lb_AccomadationType")]
    public class AccomadationType : BaseModel
    {
        public AccomadationType()
        {
            this.Apartments = new HashSet<Apartment>();
        }
    
        public int Id { get; set; }
        public string AccomadationName { get; set; }
        public string Description { get; set; }

        public virtual ICollection<Apartment> Apartments { get; set; }
    }
}