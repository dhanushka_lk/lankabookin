﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace LankaBookin.Models
{
    [Table("Lb_Facility")]
    public class Facility : BaseModel
    {
        public Facility()
        {
            this.Apartments = new HashSet<Apartment>();
        }

        public int Id { get; set; }
        public string FacilityName { get; set; }
        public string Description { get; set; }

        public virtual ICollection<Apartment> Apartments { get; set; }
    }
}