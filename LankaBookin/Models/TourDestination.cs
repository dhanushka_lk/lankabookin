﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace LankaBookin.Models
{
    [Table("Lb_TourDestination")]
    public class TourDestination : BaseModel
    {
        public int Id { get; set; }
        public int TourId { get; set; }
        public int StepId { get; set; }
        public int CityId { get; set; }
        public bool IsDesCity { get; set; }

        public virtual Tour Tour { get; set; }
        public virtual City City { get; set; }
    }
}