﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace LankaBookin.Models
{
    [Table("Lb_District")]
    public class District:BaseModel
    {
        public District()
        {
            this.City = new HashSet<City>();
        }
    
        public int Id { get; set; }
        public string DistrictName { get; set; }
        public string Description { get; set; }
     
        public virtual ICollection<City> City { get; set; }
    }
}