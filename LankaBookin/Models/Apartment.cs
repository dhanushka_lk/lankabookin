﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace LankaBookin.Models
{
    [Table("Lb_Apartment")]
    public class Apartment:BaseModel
    {
        public Apartment()
        {
            this.ApartmentImages = new HashSet<ApartmentImage>();
            this.RoomsBook = new HashSet<RoomsBook>();
            this.Rating = new HashSet<Rating>();
            this.UserWishList = new HashSet<UserWishList>();
            this.BookingRequest = new HashSet<BookingRequest>();
            this.Facilities = new HashSet<Facility>();
            this.Accomadations = new HashSet<AccomadationType>();
        }
    
        public int Id { get; set; }
        public string Name { get; set; }
        public Nullable<int> Star { get; set; }
        public decimal Rate { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string Description { get; set; }
        public bool IsPerRoom { get; set; }
        public int RoomCount { get; set; }
        public decimal Price { get; set; }
        public int MaxHeadCount { get; set; }
        public int UserId { get; set; }
        public int CityId { get; set; }
        public Nullable<int> LandmarkId { get; set; }
    
        public virtual City City { get; set; }
        public virtual User User { get; set; }
        public virtual Landmark Landmark { get; set; }
        public virtual ICollection<ApartmentImage> ApartmentImages { get; set; }
        public virtual ICollection<RoomsBook> RoomsBook { get; set; }
        public virtual ICollection<Rating> Rating { get; set; }
        public virtual ICollection<UserWishList> UserWishList { get; set; }
        public virtual ICollection<BookingRequest> BookingRequest { get; set; }
        public virtual ICollection<Facility> Facilities { get; set; }
        public virtual ICollection<AccomadationType> Accomadations { get; set; }
    }
}