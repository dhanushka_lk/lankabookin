﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace LankaBookin.Models
{
    [Table("Lb_Rating")]
    public class Rating : BaseModel
    {
        public int Id { get; set; }
        public Nullable<int> ApartmentId { get; set; }
        public Nullable<int> UserId { get; set; }
        public int Rate { get; set; }

        public virtual Apartment Apartment { get; set; }
        public virtual User Lb_User { get; set; }
    }
}