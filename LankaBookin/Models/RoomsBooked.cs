﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace LankaBookin.Models
{
    [Table("Lb_RoomsBook")]
    public class RoomsBook : BaseModel
    {
        public int Id { get; set; }
        public int ApartmentId { get; set; }
        public System.DateTime Date { get; set; }
        public int RoomCount { get; set; }

        public virtual Apartment Apartment { get; set; }
    }
}