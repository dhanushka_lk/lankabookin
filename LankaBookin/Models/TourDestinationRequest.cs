﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace LankaBookin.Models
{
    [Table("Lb_TourDestinationRequest")]
    public class TourDestinationRequest : BaseModel
    {
        public int Id { get; set; }
        public int TourRequestId { get; set; }
        public int StepId { get; set; }
        public int duration { get; set; }
        public int BookingRequestId { get; set; }
        public int CityId { get; set; }

        public virtual BookingRequest Lb_BookingRequest { get; set; }
        public virtual City Lb_City { get; set; }
        public virtual TourRequest Lb_TourRequest { get; set; }
    }
}