﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace LankaBookin.Models
{
    [Table("Lb_CategoryTour")]
    public class CategoryTour : BaseModel
    {
        public int Id { get; set; }
        public int CategoryId { get; set; }
        public int TourId { get; set; }

        public virtual Category Category { get; set; }
        public virtual Tour Tour { get; set; }
    }
}