﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace LankaBookin.Models
{
    [Table("Lb_BookingRequest")]
    public class BookingRequest:BaseModel
    {
        public BookingRequest()
        {
        }
    
        public int Id { get; set; }
        public Nullable<int> ApartmentId { get; set; }
        public decimal Price { get; set; }
        public int UserId { get; set; }
        public System.DateTime OrderDate { get; set; }
        public System.DateTime CheckIn { get; set; }
        public System.DateTime CheckOut { get; set; }
        public int RoomsBooked { get; set; }

        public virtual Apartment Apartment { get; set; }
        public virtual User User { get; set; }

    }
}