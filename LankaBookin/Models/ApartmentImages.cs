﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace LankaBookin.Models
{
    [Table("Lb_ApartmentImage")]
    public class ApartmentImage : BaseModel
    {
        public int Id { get; set; }
        public int ApartmentId { get; set; }
        public string Path { get; set; }

        public virtual Apartment Apartment { get; set; }
    }
}