﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace LankaBookin.Models
{
    [Table("Lb_Tour")]
    public class Tour:BaseModel
    {
        public Tour()
        {
            this.CategoryTour = new HashSet<CategoryTour>();
            this.TourDestination = new HashSet<TourDestination>();
            this.TourRequest = new HashSet<TourRequest>();
        }
    
        public int Id { get; set; }
        public string TourName { get; set; }
        public int Steps { get; set; }
        public string Description { get; set; }
    
        public virtual ICollection<CategoryTour> CategoryTour { get; set; }
        public virtual ICollection<TourDestination> TourDestination { get; set; }
        public virtual ICollection<TourRequest> TourRequest { get; set; }
    }
}