﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace LankaBookin.Models
{
    [Table("Lb_City")]
    public class City:BaseModel
    {
        public City()
        {
            this.Apartment = new HashSet<Apartment>();
            this.TourDestinationRequest = new HashSet<TourDestinationRequest>();
        }
    
        public int Id { get; set; }
        public string CityName { get; set; }
        public string Description { get; set; }
        public int DistrictId { get; set; }
    
        public virtual ICollection<Apartment> Apartment { get; set; }
        public virtual District District { get; set; }
        public virtual ICollection<TourDestinationRequest> TourDestinationRequest { get; set; }
    }
}