﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace LankaBookin.Models
{
    [Table("Lb_Category")]
    public class Category:BaseModel
    {
        public Category()
        {
            this.Lb_CategoryTour = new HashSet<CategoryTour>();
        }
    
        public int Id { get; set; }
        public string CategoryName { get; set; }
        public string Description { get; set; }
        public string ImagePath { get; set; }
    
        public virtual ICollection<CategoryTour> Lb_CategoryTour { get; set; }
    }
}